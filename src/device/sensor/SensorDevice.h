#ifndef SENSORDEVICE_H
#define SENSORDEVICE_H

#include "device/Weather.h"
#include "limits"

class SensorDevice {
public:
    virtual bool isConnected() { return false; };
    virtual void update(Weather &weatherEvent) {};
    virtual float noValue() { return std::numeric_limits<int>::min(); }
};

#endif //SENSORDEVICE_H
