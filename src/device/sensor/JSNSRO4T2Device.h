#ifndef WEMOSWEATHERSIMPLE_JSNSRO4T2DEVICE_H
#define WEMOSWEATHERSIMPLE_JSNSRO4T2DEVICE_H


class JSNSRO4T2Device : public SensorDevice {

public:
    JSNSRO4T2Device(int echoPin_m = 12, int triggerPin_m = 14) {
        echoPin = echoPin_m;
        triggerPin = triggerPin_m;
        pinMode(echoPin, INPUT);
        pinMode(triggerPin, OUTPUT);
        digitalWrite(echoPin, HIGH);
    }

    bool isConnected() override {
        return distance() != 0;
    }

    void update(Weather &weatherEvent) override {
        bool connected = isConnected();
        weatherEvent.distanceConnected = connected;
        if (connected) {
            weatherEvent.distanceCm = distance();
        }
    }
    
private:
    int echoPin;
    int triggerPin;

    int distance() {
        // Set the trigger pin to low for 2uS
        digitalWrite(echoPin, LOW);
        delayMicroseconds(2);

        // Send a 10uS high to trigger ranging - 20uS to work with version 2.0
        digitalWrite(triggerPin, HIGH);
        delayMicroseconds(20);

        // Send pin low again
        digitalWrite(triggerPin, LOW);

        int distance = pulseIn(echoPin, HIGH, 26000); // Read in times pulse
        distance = distance / 58;
        return distance;
    }
};


#endif //WEMOSWEATHERSIMPLE_JSNSRO4T2DEVICE_H
