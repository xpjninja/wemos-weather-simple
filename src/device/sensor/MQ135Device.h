#ifndef MQ135_DEVICE_H
#define MQ135_DEVICE_H

#include "SensorDevice.h"
#include <SSD1306AsciiWire.h>
#include "MQ135.h"

class MQ135Device : public SensorDevice {
public:

    MQ135Device(MQ135 *device) {
        mq135 = device;
        mq135->calibrate();
    }

    MQ135Device(uint8_t pin) {
         MQ135Device(new MQ135(pin));

    }

    bool isConnected() {
        return false;
    }

    void update(Weather &weatherEvent) {
        weatherEvent.mq135Connected = isConnected();
        weatherEvent.ro = mq135->getRo();
        weatherEvent.ratio = mq135->readRatio();
        weatherEvent.co2 = mq135->readCO2();
    }

    float getRZero(Weather event) {
        if (event.ro) {
            return event.ro;
        }
        return noValue();
    }
    float getRatio(Weather event) {
        if (event.ratio) {
            return event.ratio;
        }
        return noValue();
    }
    float getCo2(Weather event) {
        if (event.co2) {
            return event.co2;
        }
        return noValue();
    }

private:
    MQ135 *mq135;
};

#endif //MQ135_DEVICE_H
