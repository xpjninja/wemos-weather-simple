#ifndef CONFIG_WTS1_H
#define CONFIG_WTS1_H

#undef SUPPORT_OLED
#undef SUPPORT_EPAPER
#undef SUPPORT_MQTT
#undef SUPPORT_BLYNK
#undef DEEP_SLEEP

#define MQTT_BME280_TOPIC "sensors/wts1/bme280"
#define MQTT_BH1750_TOPIC "sensors/wts1/bh1750"
#define MQTT_MQ135_TOPIC "sensors/wts1/mq135"
#define MQTT_DISTANCE_TOPIC "sensors/wts1/distance"
#define MQTT_KEY "wts"

#endif //CONFIG_WTS1_H
